package controller.mvc.anonation;

import model.Customer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import validator.CustomerValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/customer.htm")
@SuppressWarnings("deprecation")
public class CustomerController extends SimpleFormController {

    private static final Logger log = Logger.getLogger(CustomerController.class.getName());

    public CustomerController() {
        setCommandClass(Customer.class);
        setCommandName("customerForm");
    }

    CustomerValidator customerValidator;

    @Autowired
    public CustomerController(CustomerValidator customerValidator) {
        this.customerValidator = customerValidator;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String processSubmit(
            @ModelAttribute("customer") Customer customer, BindingResult result, SessionStatus status) {

        customerValidator.validate(customer, result);

        if (result.hasErrors()) {
            //if validator failed
            return "CustomerForm";
        } else {
            status.setComplete();
            //form success
            return "CustomerSuccess";
        }
    }

    @Override
    protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {

        Customer customer = (Customer) command;
        log.info(customer);

        //put the model in session, so that it's able pass to another controller
        request.getSession().setAttribute("customer", customer);

        return new ModelAndView("customerSuccessRedirect");

    }

    @Override
    protected Object formBackingObject(HttpServletRequest request)
            throws Exception {

        Customer cust = new Customer();
        //Make "Spring MVC" as default checked value
        cust.setFavFramework(new String[]{"Spring MVC"});

        //Make "Make" as default radio button selected value
        cust.setSex("M");

        //make "Hibernate" as the default java skills selection
        cust.setJavaSkills("Hibernate");

        //initilize a hidden value
        cust.setSecretValue("I'm hidden value");

        return cust;
    }

    @Override
    protected Map<String, Object> referenceData(HttpServletRequest request) throws Exception {

        Map<String, Object> referenceData = new HashMap<String, Object>();

        //Data referencing for web framework checkboxes
        List<String> webFrameworkList = new ArrayList<String>();
        webFrameworkList.add("Spring MVC");
        webFrameworkList.add("Struts 1");
        webFrameworkList.add("Struts 2");
        webFrameworkList.add("JSF");
        webFrameworkList.add("Apache Wicket");
        referenceData.put("webFrameworkList", webFrameworkList);

        //Data referencing for number radiobuttons
        List<String> numberList = new ArrayList<String>();
        numberList.add("Number 1");
        numberList.add("Number 2");
        numberList.add("Number 3");
        numberList.add("Number 4");
        numberList.add("Number 5");
        referenceData.put("numberList", numberList);

        //Data referencing for country dropdown box
        Map<String, String> country = new LinkedHashMap<String, String>();
        country.put("US", "United Stated");
        country.put("CHINA", "China");
        country.put("SG", "Singapore");
        country.put("MY", "Malaysia");
        referenceData.put("countryList", country);

        //Data referencing for java skills list box
        Map<String, String> javaSkill = new LinkedHashMap<String, String>();
        javaSkill.put("Hibernate", "Hibernate");
        javaSkill.put("Spring", "Spring");
        javaSkill.put("Apache Wicket", "Apache Wicket");
        javaSkill.put("Struts", "Struts");
        referenceData.put("javaSkillsList", javaSkill);

        return referenceData;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String initForm(ModelMap model) {

        Customer cust = new Customer();
        //Make "Spring MVC" as default checked value
        cust.setFavFramework(new String[]{"Spring MVC"});

        //Make "Make" as default radio button selected value
        cust.setSex("M");

        //make "Hibernate" as the default java skills selection
        cust.setJavaSkills("Hibernate");

        //initilize a hidden value
        cust.setSecretValue("I'm hidden value");

        //command object
        model.addAttribute("customer", cust);

        //return form view
        return "CustomerForm";
    }

    @ModelAttribute("webFrameworkList")
    public List<String> populateWebFrameworkList() {

        //Data referencing for web framework checkboxes
        List<String> webFrameworkList = new ArrayList<String>();
        webFrameworkList.add("Spring MVC");
        webFrameworkList.add("Struts 1");
        webFrameworkList.add("Struts 2");
        webFrameworkList.add("JSF");
        webFrameworkList.add("Apache Wicket");

        return webFrameworkList;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @ModelAttribute("numberList")
    public List<String> populateNumberList() {

        //Data referencing for number radiobuttons
        List<String> numberList = new ArrayList<String>();
        numberList.add("Number 1");
        numberList.add("Number 2");
        numberList.add("Number 3");
        numberList.add("Number 4");
        numberList.add("Number 5");

        return numberList;
    }


    @ModelAttribute("javaSkillsList")
    public Map<String, String> populateJavaSkillList() {

        //Data referencing for java skills list box
        Map<String, String> javaSkill = new LinkedHashMap<String, String>();
        javaSkill.put("Hibernate", "Hibernate");
        javaSkill.put("Spring", "Spring");
        javaSkill.put("Apache Wicket", "Apache Wicket");
        javaSkill.put("Struts", "Struts");

        return javaSkill;
    }

    @ModelAttribute("countryList")
    public Map<String, String> populateCountryList() {

        //Data referencing for java skills list box
        Map<String, String> country = new LinkedHashMap<String, String>();
        country.put("US", "United Stated");
        country.put("CHINA", "China");
        country.put("SG", "Singapore");
        country.put("MY", "Malaysia");

        return country;
    }
}

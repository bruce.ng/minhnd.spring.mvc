package controller.mvc.anonation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/movie.htm")
public class MovieController extends AbstractController {

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public String getMovie(@PathVariable String name, ModelAndView model) {
        model.addObject("movie", name);
        return "Movie";

    }

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {

        model.addAttribute("movie", "MinhND: Spring 3 MVC Hello World");
        //movie ten file jsp
        return "Movie";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getDefaultMovie(ModelMap model) {

        model.addAttribute("movie", "this is default movie");
        return "Movie";
    }

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest arg0, HttpServletResponse arg1)
            throws Exception {
        ModelAndView model = new ModelAndView("Movie");
        model.addObject("movie", "WelcomeController");
        return model;
    }
}
/**
 * 1.	Controller – The controller class is no longer need to extends base controller like
 * AbstractController or SimpleFormController, just simply annotate the class with a @Controller annotation.
 * <p>
 * 2.	Handler Mapping – No more declaration for the handler mapping like BeanNameUrlHandlerMapping, ControllerClassNameHandlerMapping or SimpleUrlHandlerMapping, all are replaced with a standard @RequestMapping annotation.
 */

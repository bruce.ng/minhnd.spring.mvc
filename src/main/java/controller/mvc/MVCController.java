package controller.mvc;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MVCController extends AbstractController {

    @Override
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView model = new ModelAndView("HelloMVCPage");
        model.addObject("msg", "Minhnd MVC");
        return model;
    }

    /**
     * 1. ModelAndView("HelloMVCPage") – The "HelloMVCPage" will pass to Spring’s viewResolver later,
     * to indentify which view should return back to the user. (see step 6)
     *
     * 2. model.addObject("msg", "hello world") – Add a "hello world" string into a model named "msg",
     * later you can use JSP EL ${msg} to display the "hello world" string.
     * */

}
